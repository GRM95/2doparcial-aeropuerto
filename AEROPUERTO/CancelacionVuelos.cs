﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class CancelacionVuelos : Form
    {
        public BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        public CancelacionVuelos()
        {
            InitializeComponent();
        }

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorVuelo.Listar();

        }

        private void CancelacionVuelos_Load(object sender, EventArgs e)
        {
            label2.Text = null;
            Enlazar();


        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label2.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(label2.Text))
            {
                MessageBox.Show("Error, seleccione un Vuelo.");
                return flag;
            }
            else
            {
                flag = true;
                return flag;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                BE.Vuelo vuelo1 = new BE.Vuelo(int.Parse(label2.Text));
                gestorVuelo.Borrar(vuelo1);
                MessageBox.Show("Vuelo cancelado");
                Enlazar();
            }
            
        }
    }
}
