﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class ModificacionDestinos : Form
    {
        BLL.Destino gestorDestino = new BLL.Destino();
        public ModificacionDestinos()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorDestino.Listar();
        }


        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrEmpty(textBox3.Text) || string.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Seleccione un Destino y no deje los campos en blanco.");
                return flag;
            }
            else
            {
                flag = true;
                return flag;
            }
        }

        public void Enlazar()
        {
            textBox1.Text = null;
            textBox2.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox6.Text = null;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorDestino.Listar();
        }

        private void ModificacionDestinos_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                {

                    if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text) && 
                        string.IsNullOrEmpty(textBox4.Text) && string.IsNullOrEmpty(textBox6.Text) && string.IsNullOrEmpty(textBox5.Text))
                    {
                        MessageBox.Show("Ingresar datos");
                    }
                    else
                    {
                        if (textBox1.Text == "-." || textBox1.Text == ".-" || textBox2.Text == "-." || textBox2.Text == ".-" || textBox1.Text == "" ||
                            textBox2.Text == "")
                        {
                            MessageBox.Show("Ingrese datos");
                        }
                        else
                        {
                            if (decimal.Parse(textBox1.Text) < 180 && decimal.Parse(textBox1.Text) > -180)
                            {
                                if (decimal.Parse(textBox2.Text) < 85 && decimal.Parse(textBox2.Text) > -85)
                                {
                                    if (textBox1.Text == "-." || textBox1.Text == ".-" || textBox2.Text == "-." || textBox2.Text == ".-")
                                    {
                                    }
                                    else
                                    {
                                        BE.Destino dest = new BE.Destino(int.Parse(textBox6.Text),textBox3.Text,textBox4.Text,
                                                                         textBox5.Text,decimal.Parse(textBox1.Text),decimal.Parse(textBox2.Text));
                                        gestorDestino.Grabar(dest);
                                        Enlazar();

                                        textBox1.Clear();
                                        textBox2.Clear();
                                        textBox3.Clear();
                                        textBox4.Clear();
                                        textBox5.Clear();
                                        textBox6.Clear();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("La longitud es incorrcta. Debe ser menor a 85 y mayor a -85 ");
                                }
                            }
                            else
                            {
                                MessageBox.Show("La longitud es incorrcta. Debe ser menor a 85 y mayor a -85 ");

                            }
                        }
                    }
                }






            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBox5.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
