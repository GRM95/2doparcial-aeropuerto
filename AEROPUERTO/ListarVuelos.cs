﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace AEROPUERTO
{
    public partial class ListarVuelos : Form
    {
        BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        BLL.Pasajero gestorPasajero= new BLL.Pasajero();
        BLL.Vuelo_Acotado gestorVueloAcotado = new BLL.Vuelo_Acotado();
        BLL.Destino gestorDestino = new BLL.Destino();

        public decimal latitud, longitud;
        public ListarVuelos()
        {
            InitializeComponent();
        }

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
        }

        private void ListarVuelos_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = gestorVueloAcotado.Listar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string valorVuelo = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestorPasajero.Listar(int.Parse(valorVuelo));

            dataGridView3.DataSource = null;
            dataGridView3.DataSource = gestorVuelo.Listar(int.Parse(valorVuelo));

        }

        public int cupoMaximo, porcent, cupoActual;

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            List<BE.Vuelo> lista = new List<BE.Vuelo>();

            foreach (var item in gestorVuelo.Listar())
            {

                cupoMaximo = item.CupoMax;
                cupoActual = item.CupoActual;
                porcent = ((cupoActual * 100) / cupoMaximo);

                if (item.EstadoVuelo == "Finalizado")
                {
                    if (porcent < 40)
                    {
                        lista.Add(item);
                    }
                }
            }
            dataGridView4.DataSource = null;
            dataGridView4.DataSource = lista;
        }
    }
}
