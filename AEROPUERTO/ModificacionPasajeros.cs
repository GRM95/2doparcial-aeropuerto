﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace AEROPUERTO
{
    public partial class ModificacionPasajeros : Form
    {
        BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        public ModificacionPasajeros()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorPasajero.Listar();
        }


        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrEmpty(textBox3.Text) || string.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Seleccione un Pasajero y no deje los campos en blanco.");
                return flag;
            }
            else
            {
                flag = true;
                return flag;
            }
        }

        public void Enlazar()
        {
            textBox1.Text = null;
            textBox2.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorPasajero.Listar();
        }
        private void ModificacionPasajeros_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text) &&
                    string.IsNullOrEmpty(textBox4.Text))
                {
                    MessageBox.Show("Complete los campos");
                }
                else
                {
                    BE.Pasajero pasa = new BE.Pasajero(int.Parse(textBox3.Text), textBox1.Text, textBox2.Text, int.Parse(textBox4.Text));
                    gestorPasajero.Grabar(pasa);
                    Enlazar();
                }
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
