﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class AsignarVuelo : Form
    {
        BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        BLL.PasajeroViaje gestorPasajeroViaje = new BLL.PasajeroViaje();
        public AsignarVuelo()
        {
            InitializeComponent();
        }

        public void Enlazar()
        {
            label4.Text = null;
            label5.Text = null;
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView1.DataSource = gestorPasajero.Listar();
            dataGridView2.DataSource = gestorVuelo.Listar();
        }

        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(label5.Text) || string.IsNullOrEmpty(label4.Text))
            {
                MessageBox.Show("Error, seleccione Pasajero y Vuelo.");
                return flag;
            }
            else
            {
                flag = true;
                return flag;
            }
        }

        private void AsignarVuelo_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                bool flag = false;
                if (int.Parse(label6.Text) == int.Parse(label8.Text))
                {
                    MessageBox.Show("Cupo maximo del vuelo alcanzado");
                }
                else
                {
                    foreach (var item in gestorPasajeroViaje.Listar())
                    {
                        if (item.IDPasajero == int.Parse(label4.Text) && item.IDVuelo == int.Parse(label5.Text))
                        {
                            flag = true;
                        }
                    }

                    if (flag)
                    {
                        MessageBox.Show("El pasajero ya se encuentra en este vuelo");
                    }
                    else
                    {
                        BE.PasajeroViaje pasajeroViaje = new BE.PasajeroViaje(int.Parse(label5.Text), int.Parse(label4.Text));
                        gestorPasajeroViaje.Grabar(pasajeroViaje);
                        label4.Text = null;
                        label5.Text = null;
                        Enlazar();
                    }
                }

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label4.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void dataGridView2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label5.Text = dataGridView2.Rows[e.RowIndex].Cells[4].Value.ToString();
            label6.Text = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            label8.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
        }
    }
}
