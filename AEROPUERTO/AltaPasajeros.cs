﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class AltaPasajeros : Form
    {
        BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        

        public AltaPasajeros()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("Ingresar datos");
            }
            else
            {
                BE.Pasajero p = new BE.Pasajero(textBox1.Text, textBox2.Text, int.Parse(textBox3.Text));

                gestorPasajero.Grabar(p);

                MessageBox.Show("Pasajero dado de alta");
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
            }
        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void AltaPasajeros_Load(object sender, EventArgs e)
        {
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
