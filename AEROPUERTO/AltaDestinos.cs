﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class AltaDestinos : Form
    {

        BLL.Destino gestorDestino = new BLL.Destino();


        public AltaDestinos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text) && string.IsNullOrEmpty(textBox4.Text) && string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("Ingresar datos");
            }
            else
            {
                if (textBox4.Text == "-." || textBox4.Text == ".-" || textBox6.Text == "-." || textBox6.Text == ".-" || textBox4.Text == "" || textBox6.Text == "")
                {
                    MessageBox.Show("Ingrese datos");
                }
                else
                {
                    if (decimal.Parse(textBox4.Text) < 180 && decimal.Parse(textBox4.Text) > -180)
                    {
                        if (decimal.Parse(textBox6.Text) < 85 && decimal.Parse(textBox6.Text) > -85)
                        {
                            if (textBox4.Text == "-." || textBox4.Text == ".-" || textBox6.Text == "-." || textBox6.Text == ".-")
                            {
                            }
                            else
                            {
                                BE.Destino p = new BE.Destino(textBox1.Text, textBox2.Text, textBox3.Text, decimal.Parse(textBox4.Text), decimal.Parse(textBox6.Text));

                                gestorDestino.Grabar(p);

                                MessageBox.Show("Destino dado de alta");
                                textBox1.Clear();
                                textBox2.Clear();
                                textBox3.Clear();
                            }
                        }
                        else
                        {
                            MessageBox.Show("La longitud es incorrcta. Debe ser menor a 85 y mayor a -85 ");
                        }
                    }
                    else
                    {
                        MessageBox.Show("La longitud es incorrcta. Debe ser menor a 85 y mayor a -85 ");

                    }
                }
            }
        }

        public void Enlazar()
        {
            textBox1.Text = null;
            textBox2.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            textBox6.Text = null;
        }

        private void AltaDestinos_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*CultureInfo cc =
    System.Threading.Thread.CurrentThread.CurrentCulture;
            if (char.IsNumber(e.KeyChar) || e.KeyChar.ToString() == cc.NumberFormat.NumberDecimalSeparator)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }*/

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }


        }
    }
}
