﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class Aeropuerto : Form
    {
        public Aeropuerto()
        {
            InitializeComponent();
        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AltaPasajeros f2 = new AltaPasajeros();
            f2.MdiParent = this;
            f2.Show();
        }

        private void bajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BajaPasajeros f3 = new BajaPasajeros();
            f3.MdiParent = this;
            f3.Show();
        }

        private void modificaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificacionPasajeros f4 = new ModificacionPasajeros();
            f4.MdiParent = this;
            f4.Show();
        }

        private void altaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AltaDestinos f5 = new AltaDestinos();
            f5.MdiParent = this;
            f5.Show();
        }

        private void bajaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BajaDestinos f6 = new BajaDestinos();
            f6.MdiParent = this;
            f6.Show();
        }

        private void modificaciónToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ModificacionDestinos f7 = new ModificacionDestinos();
            f7.MdiParent = this;
            f7.Show();
        }

        private void altaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AltaVuelos f8 = new AltaVuelos();
            f8.MdiParent = this;
            f8.Show();
        }

        private void cancelaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CancelacionVuelos f9 = new CancelacionVuelos();
            f9.MdiParent = this;
            f9.Show();
        }

        private void asignarVueloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AsignarVuelo f10 = new AsignarVuelo();
            f10.MdiParent = this;
            f10.Show();
        }

        private void listarVuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListarVuelos f11 = new ListarVuelos();
            f11.MdiParent = this;
            f11.Show();
        }
    }
}
