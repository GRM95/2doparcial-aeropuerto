﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;


namespace AEROPUERTO
{
    public partial class AltaVuelos : Form
    {
        public BLL.Avion gestorAvion = new BLL.Avion();
        public BLL.Destino gestorDestino = new BLL.Destino();
        public BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        public BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        public AltaVuelos()
        {
            InitializeComponent();
        }

        public void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorAvion.Listar();

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestorDestino.Listar();

            dataGridView3.DataSource = null;
            dataGridView3.DataSource = gestorDestino.Listar();

        }

        private void AltaVuelos_Load(object sender, EventArgs e)
        {
            label2.Text = null;
            label3.Text = null;
            label4.Text = null;
            label5.Text = null;
            label8.Text = null;
            label9.Text = null;
            label11.Text = null;
            label10.Text = null;
            label20.Text = null;
            label21.Text = null;
            label22.Text = null;
            label23.Text = null;
            label24.Text = null;
            label25.Text = null;
            label26.Text = null;
            label27.Text = null;



            Enlazar();

        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label2.Text = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            label3.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            label4.Text = dataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString();
            label5.Text = dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString();
            label11.Text = dataGridView2.Rows[e.RowIndex].Cells[4].Value.ToString();
            label10.Text = dataGridView2.Rows[e.RowIndex].Cells[5].Value.ToString();

            if (label22.Text == label4.Text)
            {
                if (label4.Text == "Argentina")
                {
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = true;
                    radioButton2.Checked = true;
                }
                else
                {
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = false;
                    radioButton1.Checked = false;
                    radioButton2.Checked = false;
                }
            }
            else
            {
                if (label22.Text == "Argentina" || label4.Text == "Argentina")
                {
                    if (label22.Text == "Peru" || label4.Text == "Peru")
                    {
                        radioButton1.Enabled = false;
                        radioButton2.Enabled = true;
                        radioButton2.Checked = true;
                    }
                    else
                    {
                        radioButton1.Enabled = true;
                        radioButton2.Enabled = true;
                        radioButton1.Checked = false;
                        radioButton2.Checked = false;
                    }
                }
            }
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label20.Text = dataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString();
            label21.Text = dataGridView3.Rows[e.RowIndex].Cells[1].Value.ToString();
            label22.Text = dataGridView3.Rows[e.RowIndex].Cells[2].Value.ToString();
            label23.Text = dataGridView3.Rows[e.RowIndex].Cells[3].Value.ToString();
            label8.Text = dataGridView3.Rows[e.RowIndex].Cells[4].Value.ToString();
            label9.Text = dataGridView3.Rows[e.RowIndex].Cells[5].Value.ToString();

            if (label22.Text == label4.Text)
            {
                if (label4.Text == "Argentina")
                {
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = true;
                    radioButton2.Checked = true;
                }
                else
                {
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = false;
                    radioButton1.Checked = false;
                    radioButton2.Checked = false;
                }
            }
            else
            {
                if (label22.Text == "Argentina" || label4.Text == "Argentina")
                {
                    if (label22.Text == "Peru" || label4.Text == "Peru")
                    {
                        radioButton1.Enabled = false;
                        radioButton2.Enabled = true;
                        radioButton2.Checked = true;
                    }
                    else
                    {
                        radioButton1.Enabled = true;
                        radioButton2.Enabled = true;
                        radioButton1.Checked = false;
                        radioButton2.Checked = false;
                    }
                }
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label24.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            label25.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            label26.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label7.Enabled = true;
            label7.Visible = true;
            dateTimePicker3.Enabled = true;
            dateTimePicker3.Visible = true;
            dateTimePicker5.Enabled = true;
            dateTimePicker5.Visible = true;

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            label7.Enabled = false;
            label7.Visible = false;
            dateTimePicker3.Enabled = false;
            dateTimePicker3.Visible = false;
            dateTimePicker5.Enabled = false;
            dateTimePicker5.Visible = false;

        }

        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(label2.Text) || string.IsNullOrEmpty(label20.Text) || string.IsNullOrEmpty(label24.Text))
            {
                if (label5.Text == label23.Text)
                {
                    MessageBox.Show("El origen no puede ser el mismo que la salida. Seleccione otro aeropuerto en todo caso.");
                    return flag;
                }
                else
                {
                    MessageBox.Show("Seleccione Origen, Destino y Avion");
                    return flag;
                }

            }
            else
            {
                if (radioButton1.Checked == false && radioButton2.Checked == false)
                {
                    flag = false;
                    MessageBox.Show("Verificar la Escala");
                    return flag;
                }
                if (dateTimePicker1.Value < DateTime.Today || dateTimePicker2.Value < DateTime.Today)
                {
                    flag = false;
                    MessageBox.Show("La fecha colocada es incorrecta");
                    return flag;
                }
                else
                {
                    flag = true;
                    return flag;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                string tipoVuelito;
                if (label4.Text == label22.Text && label4.Text == "Argentina")
                {
                    tipoVuelito = "Cabotaje";
                }
                else
                {
                    tipoVuelito = "Internacional";
                }

                if (dateTimePicker1.Value != DateTime.Today && dateTimePicker2.Value != DateTime.Today && dateTimePicker3.Value != DateTime.Today)
                {
                    if (radioButton1.Checked)
                    {
                        //Primer vuelo hacia Peru
                        BE.Vuelo vuelo1 = new BE.Vuelo(int.Parse(label10.Text), 2, int.Parse(label24.Text), dateTimePicker1.Value.ToShortDateString(),
                                                        dateTimePicker3.Value.ToShortDateString(), dateTimePicker4.Value.ToShortTimeString(),
                                                        dateTimePicker5.Value.ToShortTimeString(), radioButton2.Checked,tipoVuelito, "Disponible",
                                                        int.Parse(label25.Text), 0);
                        //Segundo vuelo hacia Destino
                        BE.Vuelo vuelo2 = new BE.Vuelo(2, int.Parse(label9.Text), int.Parse(label24.Text), dateTimePicker3.Value.ToShortDateString(),
                                                        dateTimePicker2.Value.ToShortDateString(), dateTimePicker5.Value.ToShortTimeString(),
                                                        dateTimePicker6.Value.ToShortTimeString(), radioButton2.Checked,tipoVuelito, "Disponible",
                                                        int.Parse(label25.Text), 0);

                        gestorVuelo.Grabar(vuelo1);
                        gestorVuelo.Grabar(vuelo2);

                        MessageBox.Show("Se crearon 2 viajes");
                    }
                    else
                    {
                        BE.Vuelo p = new BE.Vuelo(int.Parse(label10.Text),int.Parse(label9.Text),int.Parse(label24.Text),dateTimePicker1.Value.ToShortDateString(),
                                                    dateTimePicker2.Value.ToShortDateString(),dateTimePicker4.Value.ToShortDateString(),dateTimePicker6.Value.ToShortDateString(),
                                                    radioButton2.Checked,tipoVuelito,"Disponible",int.Parse(label25.Text),0);

                        gestorVuelo.Grabar(p);

                        MessageBox.Show("Vuelo dado de alta");
                    }
                }
                else
                {
                    MessageBox.Show("La fecha tiene que ser posterior al día actual");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(label20.Text) || string.IsNullOrEmpty(label21.Text))
            {
                MessageBox.Show("Liste y seleccione el vuelo para ver su destino.");
            }
            else
            {
                string servicio = "chrome.exe";
                string web = ("https://google.com.ar/maps/place/?q=" + label20.Text + "," + label21.Text);
                Process.Start(servicio, web);
            }
        }
    }
}
