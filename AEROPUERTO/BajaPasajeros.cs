﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class BajaPasajeros : Form
    {
        BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        public BajaPasajeros()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorPasajero.Listar();
        }

        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(label2.Text))
            {
                MessageBox.Show("Error, seleccione un Pasajero.");
                return flag;
            }
            else
            {
                flag = true;
                return flag;
            }
        }

        public void Enlazar()
        {
            label2.Text = null;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorPasajero.Listar();
        }


        private void BajaPasajeros_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                BE.Pasajero pasa = new BE.Pasajero(int.Parse(label2.Text));
                gestorPasajero.Borrar(pasa);
                MessageBox.Show("Pasajero dado de baja");
                Enlazar();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label2.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();

        }
    }
}
