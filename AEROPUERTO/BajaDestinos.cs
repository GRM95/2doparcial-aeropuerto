﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEROPUERTO
{
    public partial class BajaDestinos : Form
    {
        BLL.Destino gestorDestino = new BLL.Destino();
        public BajaDestinos()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorDestino.Listar();
        }

        public void Enlazar()
        {
            label7.Text = null;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorDestino.Listar();
        }

        public bool Check()
        {
            bool flag = false;
            if (string.IsNullOrEmpty(label7.Text))
            {
                MessageBox.Show("Error, seleccione un Destino.");
                return flag;
            }
            else
            {
                flag = true;
                return flag;
            }
        }

        private void BajaDestinos_Load(object sender, EventArgs e)
        {
            Enlazar();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label7.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                BE.Destino dest= new BE.Destino(int.Parse(label7.Text));
                gestorDestino.Borrar(dest);
                MessageBox.Show("Destino cancelado");
                Enlazar();
            }
        }
    }
}
