﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Vuelo
    {
        DAL.MP_VUELO mp = new DAL.MP_VUELO();

        public void Grabar(BE.Vuelo vuelo)
        {
            if (vuelo.IDVuelo == 0)
            {
                mp.Insertar(vuelo);

            }
        }

        public void Borrar(BE.Vuelo vuelo)
        {
            mp.Borrar(vuelo);
        }


        public List<BE.Vuelo> Listar(int id = 0)
        {
            if (id == 0)
            {
                return mp.Listar();
            }
            else
            {
                return mp.ListarPorVuelo(id);
            }
            
        }
    }
}
