﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Avion
    {
        DAL.MP_AVION mp = new DAL.MP_AVION();

        public List<BE.Avion> Listar()
        {
            return mp.Listar();
        }
    }
}
