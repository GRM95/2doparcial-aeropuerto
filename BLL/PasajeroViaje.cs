﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PasajeroViaje
    {
        DAL.MP_PASAJEROVIAJE mp = new DAL.MP_PASAJEROVIAJE();

        public void Grabar(BE.PasajeroViaje pasajeroviaje)
        {
                mp.Insertar(pasajeroviaje);
        }

        public List<BE.PasajeroViaje> Listar()
        {
            return mp.Listar();
        }
    }
}
