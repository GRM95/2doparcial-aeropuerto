﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Pasajero
    {
        DAL.MP_PASAJERO mp = new DAL.MP_PASAJERO();
        
        public void Grabar(BE.Pasajero pasajero)
        {
            if (pasajero.IDPasajero == 0)
            {
                mp.Insertar(pasajero);

            }
            else
            {
                mp.Editar(pasajero);
            }

        }

        public void Borrar(BE.Pasajero pasajero)
        {
            mp.Borrar(pasajero);
        }

        public List<BE.Pasajero> Listar(int id = 0)
        {
            if (id == 0)
            {
                return mp.Listar();
            }
            else
            {
                return mp.ListarPorVuelo(id);
            }
            
        }
    }
}
