﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Pasajero
    {

        public Pasajero(int i)
        {
            IDPasajero = i;
        }
        public Pasajero(int c, string n, string a, int I)
        {
            iDPasajero = I;
            nombre = n;
            apellido = a;
            dni = c;
        }

        public Pasajero(string n, string a, int c)
        {
            nombre = n;
            apellido = a;
            dni = c;
        }

        private int iDPasajero;

        public int IDPasajero
        {
            get { return iDPasajero; }
            set { iDPasajero = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private int dni;

        public int DNI
        {
            get { return dni; }
            set { dni = value; }
        }

    }
}
