﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Vuelo
    {
        public Vuelo(int ido, int idd, int ida, string fd, string fa, string hd, string ha, bool e, string tv, string ev, int cupomax, int cupoact)
        {
            iDOrigen = ido;
            iDDestino = idd;
            iDAvion = ida;
            fechaDespegue = fd;
            fechaArrivo = fa;
            horaDespegue = hd;
            horaArrivo = ha;
            escala = e;
            estadoVuelo = ev;
            tipoVuelo = tv;
            cupoMax = cupomax;
            cupoActual = cupoact;
        }

        public Vuelo(int ido, int idd, int ida, int idv, string fd, string fa, string hd, string ha, bool e, string ev, int cupomax, int cupoactual)
        {
            iDVuelo = idv;
            iDOrigen = ido;
            iDDestino = idd;
            iDAvion = ida;
            fechaDespegue = fd;
            fechaArrivo = fa;
            horaDespegue = hd;
            horaArrivo = ha;
            escala = e;
            estadoVuelo = ev;
            if (e)
            {
                tipoVuelo = "Internacional";
            }
            else
            {
                tipoVuelo = "Cabotaje";
            }
            cupoMax = cupomax;
            cupoActual = cupoactual;

        }

        public Vuelo(int ido, int idd, int ida, int idv, string fd, string fa, string hd, string ha, string tv, bool e, string ev, int cupomax, int cupoactual)
        {
            iDOrigen = ido;
            iDDestino = idd;
            iDAvion = ida;
            iDVuelo = idv;
            fechaDespegue = fd;
            fechaArrivo = fa;
            horaDespegue = hd;
            horaArrivo = ha;
            escala = e;
            estadoVuelo = ev;
            tipoVuelo = tv;
            cupoMax = cupomax;
            cupoActual = cupoactual;
        }


        public Vuelo(int idv)
        {
            iDVuelo = idv;
        }

        private int cupoActual;

        public int CupoActual
        {
            get { return cupoActual; }
            set { cupoActual = value; }
        }


        private int cupoMax;

        public int CupoMax
        {
            get { return cupoMax; }
            set { cupoMax = value; }
        }


        private string horaDespegue;

        public string HoraDespuegue
        {
            get { return horaDespegue; }
            set { horaDespegue = value; }
        }

        private string horaArrivo;

        public string HoraArrivo
        {
            get { return horaArrivo; }
            set { horaArrivo = value; }
        }



        private int iDVuelo;

        public int IDVuelo
        {
            get { return iDVuelo; }
            set { iDVuelo = value; }
        }

        private int iDOrigen;

        public int IDOrigen
        {
            get { return iDOrigen; }
            set { iDOrigen = value; }
        }

        private int iDDestino;

        public int IDDestino
        {
            get { return iDDestino; }
            set { iDDestino = value; }
        }

        private int iDAvion;

        public int IDAvion
        {
            get { return iDAvion; }
            set { iDAvion = value; }
        }

        private string fechaDespegue;

        public string FechaDespegue
        {
            get { return fechaDespegue; }
            set { fechaDespegue = value; }
        }

        private string fechaArrivo;

        public string FechaArrivo
        {
            get { return fechaArrivo; }
            set { fechaArrivo = value; }
        }

        private string tipoVuelo;

        public string TipoVuelo
        {
            get { return tipoVuelo; }
            set { tipoVuelo = value; }
        }

        private bool escala;

        public bool Escala
        {
            get { return escala; }
            set { escala = value; }
        }

        private string estadoVuelo;

        public string EstadoVuelo
        {
            get { return estadoVuelo; }
            set { estadoVuelo = value; }
        }


    }
}
