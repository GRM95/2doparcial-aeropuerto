﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Vuelo_Acotado
    {

        public Vuelo_Acotado(int ido, string fd, string tv)
        {
            iDOrigen = ido;
            fechaDespegue = fd;
            tipoVuelo = tv;
        }

        private int iDOrigen;

        public int IDOrigen
        {
            get { return iDOrigen; }
            set { iDOrigen = value; }
        }

        private string fechaDespegue;

        public string FechaDespegue
        {
            get { return fechaDespegue; }
            set { fechaDespegue = value; }
        }

        private string tipoVuelo;

        public string TipoVuelo
        {
            get { return tipoVuelo; }
            set { tipoVuelo = value; }
        }


    }
}
