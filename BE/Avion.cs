﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Avion
    {

        public Avion(int numA, int cM, string nA)
        {
            numAvion = numA;
            cupoMax = cM;
            nombreAvion = nA;
        }


        private int numAvion;

        public int NumAvion
        {
            get { return numAvion; }
            set { numAvion = value; }
        }

        private int cupoMax;

        public int CupoMax
        {
            get { return cupoMax; }
            set { cupoMax = value; }
        }


        private string nombreAvion;

        public string NombreAvion
        {
            get { return nombreAvion; }
            set { nombreAvion = value; }
        }



    }
}
