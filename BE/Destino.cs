﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Destino
    {

        public Destino(int id, string p, string c, string a, decimal la, decimal lo)
        {
            iDLugar = id;
            pais = p;
            ciudad = c;
            aeropuerto = a;
            latitud = la;
            longitud = lo;
        }

        public Destino(string p, string c, string a, decimal la, decimal lo)
        {
            pais = p;
            ciudad = c;
            aeropuerto = a;
            latitud = la;
            longitud = lo;
        }

        public Destino(int id)
        {
            iDLugar = id;
        }

        private decimal latitud;

        public decimal Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        private decimal longitud;

        public decimal Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }


        private string pais;

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string ciudad;

        public string Ciudad
        {
            get { return ciudad; }
            set { ciudad = value; }
        }

        private string aeropuerto;

        public string Aeropuerto
        {
            get { return aeropuerto; }
            set { aeropuerto = value; }
        }

        private int iDLugar;

        public int IDLugar
        {
            get { return iDLugar; }
            set { iDLugar = value; }
        }


    }
}
