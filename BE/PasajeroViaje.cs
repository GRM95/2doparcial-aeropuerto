﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class PasajeroViaje
    {
        public PasajeroViaje(int idv, int idp)
        {
            iDVuelo = idv;
            iDPasajero = idp;
        }

        private int iDVuelo;

        public int IDVuelo
        {
            get { return iDVuelo; }
            set { iDVuelo = value; }
        }

        private int iDPasajero;

        public int IDPasajero
        {
            get { return iDPasajero; }
            set { iDPasajero = value; }
        }

    }
}
