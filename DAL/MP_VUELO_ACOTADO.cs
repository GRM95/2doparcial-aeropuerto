﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
    public class MP_VUELO_ACOTADO
    {
        public bool flag;

        private Acceso acceso = new Acceso();

        public List<BE.Vuelo_Acotado> Listar()
        {
            List<BE.Vuelo_Acotado> lista = new List<BE.Vuelo_Acotado>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.LeerQuery("SELECT IDDestino,FechaDespegue,TipoVuelo FROM Vuelo");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Vuelo_Acotado p = new BE.Vuelo_Acotado(int.Parse(registro[0].ToString()),registro[1].ToString(),registro[2].ToString());
                lista.Add(p);
            }
            return lista;
        }
    }
}
