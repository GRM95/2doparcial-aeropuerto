﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
    public class MP_VUELO
    {
        public bool flag;

        private Acceso acceso = new Acceso();


        public void Insertar(BE.Vuelo vuelo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDOrigen", vuelo.IDOrigen));
            parameters.Add(acceso.CrearParametro("@IDDestino", vuelo.IDDestino));
            parameters.Add(acceso.CrearParametro("@IDAvion", vuelo.IDAvion));
            parameters.Add(acceso.CrearParametro("@FechaDespegue", vuelo.FechaDespegue));
            parameters.Add(acceso.CrearParametro("@FechaArrivo", vuelo.FechaArrivo));
            parameters.Add(acceso.CrearParametro("@HoraDespegue", vuelo.HoraDespuegue));
            parameters.Add(acceso.CrearParametro("@HoraArrivo", vuelo.HoraArrivo));
            parameters.Add(acceso.CrearParametro("@TipoVuelo", vuelo.TipoVuelo));
            parameters.Add(acceso.CrearParametro("@Escala", vuelo.Escala));
            parameters.Add(acceso.CrearParametro("@EstadoVuelo", vuelo.EstadoVuelo));
            parameters.Add(acceso.CrearParametro("@CupoMax", vuelo.CupoMax));
            parameters.Add(acceso.CrearParametro("@CupoActual", vuelo.CupoActual));


            acceso.Escribir("VUELO_ALTA", parameters);


            acceso.Cerrar();
        }

        public void Borrar(BE.Vuelo vuelo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDVuelo", vuelo.IDVuelo));
            acceso.Escribir("VUELO_CANCELAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Vuelo> ListarPorVuelo(int id)
        {
            List<BE.Vuelo> lista = new List<BE.Vuelo>();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDVuelo", id));

            DataTable tabla = acceso.Leer("BUSCAR_VUELO", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Vuelo p = new BE.Vuelo(int.Parse(registro[0].ToString()), int.Parse(registro[1].ToString()), int.Parse(registro[2].ToString()),
                           int.Parse(registro[3].ToString()), registro[4].ToString(), registro[5].ToString(), registro[6].ToString(),
                           registro[7].ToString(), registro[8].ToString(), bool.Parse(registro[9].ToString()), registro[10].ToString(),
                           int.Parse(registro[11].ToString()), int.Parse(registro[12].ToString()));

                lista.Add(p);
            }
            return lista;
        }

        public List<BE.Vuelo> Listar()
        {
            List<BE.Vuelo> lista = new List<BE.Vuelo>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("VUELO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Vuelo p = new BE.Vuelo(int.Parse(registro[0].ToString()), int.Parse(registro[1].ToString()), int.Parse(registro[2].ToString()),
                                           int.Parse(registro[3].ToString()), registro[4].ToString(), registro[5].ToString(), registro[6].ToString(),
                                           registro[7].ToString(), registro[8].ToString(), bool.Parse(registro[9].ToString()), registro[10].ToString(),
                                           int.Parse(registro[11].ToString()), int.Parse(registro[12].ToString()));
                lista.Add(p);
            }
            return lista;
        }
    }
}
