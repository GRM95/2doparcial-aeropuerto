﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
    public class MP_AVION
    {
        public bool flag;

        private Acceso acceso = new Acceso();

        public List<BE.Avion> Listar()
        {
            List<BE.Avion> lista = new List<BE.Avion>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("AVION_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Avion p = new BE.Avion(int.Parse(registro[0].ToString()),int.Parse(registro[1].ToString()),registro[2].ToString());
                lista.Add(p);
            }
            return lista;
        }
    }
}
