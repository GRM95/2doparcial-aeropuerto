﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
    public class MP_PASAJEROVIAJE
    {
        public bool flag;

        private Acceso acceso = new Acceso();

        public void Insertar(BE.PasajeroViaje pasajeroViaje)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDVuelo", pasajeroViaje.IDVuelo));
            parameters.Add(acceso.CrearParametro("@IDPasajero", pasajeroViaje.IDPasajero));
            
            acceso.Escribir("PASAJEROVIAJE_INSERTAR", parameters);


            acceso.Cerrar();
        }

        public List<BE.PasajeroViaje> Listar()
        {
            List<BE.PasajeroViaje> lista = new List<BE.PasajeroViaje>();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("PASAJEROVIAJE_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.PasajeroViaje p = new BE.PasajeroViaje(int.Parse(registro[0].ToString()),int.Parse(registro[1].ToString()));
                lista.Add(p);
            }
            return lista;
        }
    }
}
