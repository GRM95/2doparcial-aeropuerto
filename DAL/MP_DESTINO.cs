﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
    public class MP_DESTINO
    {


        public bool flag;

        private Acceso acceso = new Acceso();

        public void Insertar(BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Pais", destino.Pais));
            parameters.Add(acceso.CrearParametro("@Ciudad", destino.Ciudad));
            parameters.Add(acceso.CrearParametro("@Aeropuerto", destino.Aeropuerto));
            parameters.Add(acceso.CrearParametro("@Latitud", destino.Latitud));
            parameters.Add(acceso.CrearParametro("@Longitud", destino.Longitud));
            acceso.Escribir("LUGAR_INSERTAR", parameters);


            acceso.Cerrar();
        }

        public void Editar(BE.Destino destino)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDLugar", destino.IDLugar));
            parameters.Add(acceso.CrearParametro("@Pais", destino.Pais));
            parameters.Add(acceso.CrearParametro("@Ciudad", destino.Ciudad));
            parameters.Add(acceso.CrearParametro("@Aeropuerto", destino.Aeropuerto));
            parameters.Add(acceso.CrearParametro("@Latitud", destino.Latitud));
            parameters.Add(acceso.CrearParametro("@Longitud", destino.Longitud));
            acceso.Escribir("LUGAR_MODIFICAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDLugar", destino.IDLugar));
            acceso.Escribir("LUGAR_BORRAR", parameters);

            acceso.Cerrar();
        }


        public  List<BE.Destino> Listar()
        {
            List<BE.Destino> lista = new List<BE.Destino>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("LUGAR_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Destino p = new BE.Destino(int.Parse(registro[0].ToString()), registro[1].ToString(), registro[2].ToString(), registro[3].ToString(), decimal.Parse(registro[4].ToString()), decimal.Parse(registro[5].ToString()));
                lista.Add(p);
            }
            return lista;
        }
    }
}
