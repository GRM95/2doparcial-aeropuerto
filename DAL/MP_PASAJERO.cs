﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
    public class MP_PASAJERO
    {

        public bool flag;

        private Acceso acceso = new Acceso();

        public void Insertar(BE.Pasajero pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@dni", pasajero.DNI));
            parameters.Add(acceso.CrearParametro("@nom", pasajero.Nombre));
            parameters.Add(acceso.CrearParametro("@apell", pasajero.Apellido));
            acceso.Escribir("PASAJERO_INSERTAR", parameters);


            acceso.Cerrar();
        }

        public void Editar(BE.Pasajero pasajero)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDPasajero", pasajero.IDPasajero));
            parameters.Add(acceso.CrearParametro("@nom", pasajero.Nombre));
            parameters.Add(acceso.CrearParametro("@apell", pasajero.Apellido));
            parameters.Add(acceso.CrearParametro("@dni", pasajero.DNI));
            
            acceso.Escribir("PASAJERO_MODIFICAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Pasajero pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@idPasajero", pasajero.IDPasajero));
            acceso.Escribir("PASAJERO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public List<BE.Pasajero> ListarPorVuelo(int id)
        {
            List<BE.Pasajero> lista = new List<BE.Pasajero>();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IDVuelo", id));
            
            DataTable tabla = acceso.Leer("PASAJERO_VUELO", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Pasajero p = new BE.Pasajero(int.Parse(registro[0].ToString()), registro[1].ToString(), registro[2].ToString(), int.Parse(registro[3].ToString()));
                lista.Add(p);
            }
            return lista;
        }

        public List<BE.Pasajero> Listar()
        {
            List<BE.Pasajero> lista = new List<BE.Pasajero>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("PASAJERO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Pasajero p = new BE.Pasajero(int.Parse(registro[0].ToString()),registro[1].ToString(),registro[2].ToString(),int.Parse(registro[3].ToString()));
                lista.Add(p);
            }
            return lista;
        }
    }
}
