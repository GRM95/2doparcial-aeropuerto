USE [master]
GO
/****** Object:  Database [2doParcial-Aeropuerto-LUG]    Script Date: 17/11/2020 08:45:37 p. m. ******/
CREATE DATABASE [2doParcial-Aeropuerto-LUG]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'2doParcial-Aeropuerto-LUG', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\2doParcial-Aeropuerto-LUG.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'2doParcial-Aeropuerto-LUG_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\2doParcial-Aeropuerto-LUG_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [2doParcial-Aeropuerto-LUG].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET ARITHABORT OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET  DISABLE_BROKER 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET  MULTI_USER 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET DB_CHAINING OFF 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET QUERY_STORE = OFF
GO
USE [2doParcial-Aeropuerto-LUG]
GO
/****** Object:  Table [dbo].[Avion]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Avion](
	[NumAvion] [int] NOT NULL,
	[CupoMax] [int] NOT NULL,
	[NombreAvion] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lugar]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lugar](
	[IDLugar] [int] NOT NULL,
	[Pais] [varchar](50) NOT NULL,
	[Ciudad] [varchar](50) NOT NULL,
	[Aeropuerto] [varchar](50) NOT NULL,
	[Latitud] [money] NOT NULL,
	[Longitud] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pasajero]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pasajero](
	[Dni] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[IDPasajero] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vuelo]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vuelo](
	[IDOrigen] [int] NOT NULL,
	[IDDestino] [int] NOT NULL,
	[IDAvion] [int] NOT NULL,
	[IDVuelo] [int] NOT NULL,
	[FechaDespegue] [varchar](50) NOT NULL,
	[FechaArrivo] [varchar](50) NOT NULL,
	[HoraDespegue] [varchar](50) NOT NULL,
	[HoraArrivo] [varchar](50) NOT NULL,
	[TipoVuelo] [varchar](50) NOT NULL,
	[Escala] [bit] NOT NULL,
	[EstadoVuelo] [varchar](50) NOT NULL,
	[CupoMax] [int] NOT NULL,
	[CupoActual] [int] NOT NULL,
 CONSTRAINT [PK_Vuelo] PRIMARY KEY CLUSTERED 
(
	[IDVuelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vuelo_Pasajero]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vuelo_Pasajero](
	[IDPasajero] [int] NOT NULL,
	[IDVuelo] [int] NOT NULL,
 CONSTRAINT [PK_Vuelo_Pasajero] PRIMARY KEY CLUSTERED 
(
	[IDPasajero] ASC,
	[IDVuelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Avion] ([NumAvion], [CupoMax], [NombreAvion]) VALUES (1, 5, N'Alpha')
INSERT [dbo].[Avion] ([NumAvion], [CupoMax], [NombreAvion]) VALUES (2, 5, N'Omega')
INSERT [dbo].[Avion] ([NumAvion], [CupoMax], [NombreAvion]) VALUES (3, 5, N'Beta')
GO
INSERT [dbo].[Lugar] ([IDLugar], [Pais], [Ciudad], [Aeropuerto], [Latitud], [Longitud]) VALUES (1, N'Argentina', N'Buenos Aires', N'JN', -20.0000, -23.0000)
INSERT [dbo].[Lugar] ([IDLugar], [Pais], [Ciudad], [Aeropuerto], [Latitud], [Longitud]) VALUES (2, N'Peru', N'Lima', N'A', -100.0000, 84.0000)
GO
INSERT [dbo].[Pasajero] ([Dni], [Nombre], [Apellido], [IDPasajero]) VALUES (11111, N'Gonzalo', N'Romero', 1)
INSERT [dbo].[Pasajero] ([Dni], [Nombre], [Apellido], [IDPasajero]) VALUES (22222, N'Christian', N'Parkinson', 2)
INSERT [dbo].[Pasajero] ([Dni], [Nombre], [Apellido], [IDPasajero]) VALUES (33333, N'Bob', N'A', 3)
INSERT [dbo].[Pasajero] ([Dni], [Nombre], [Apellido], [IDPasajero]) VALUES (44444, N'Alex', N'B', 4)
INSERT [dbo].[Pasajero] ([Dni], [Nombre], [Apellido], [IDPasajero]) VALUES (55554, N'Murray', N'C', 5)
GO
INSERT [dbo].[Vuelo] ([IDOrigen], [IDDestino], [IDAvion], [IDVuelo], [FechaDespegue], [FechaArrivo], [HoraDespegue], [HoraArrivo], [TipoVuelo], [Escala], [EstadoVuelo], [CupoMax], [CupoActual]) VALUES (1, 1, 1, 1, N'10/10/1995', N'11/10/1995', N'10:30a.m', N'10:30p.m', N'Cabotaje', 0, N'Finalizado', 5, 3)
INSERT [dbo].[Vuelo] ([IDOrigen], [IDDestino], [IDAvion], [IDVuelo], [FechaDespegue], [FechaArrivo], [HoraDespegue], [HoraArrivo], [TipoVuelo], [Escala], [EstadoVuelo], [CupoMax], [CupoActual]) VALUES (1, 1, 1, 2, N'10/10/2002', N'11/10/2002', N'11:30a.m', N'11:30p.m', N'Cabotaje', 0, N'Finalizado', 5, 5)
INSERT [dbo].[Vuelo] ([IDOrigen], [IDDestino], [IDAvion], [IDVuelo], [FechaDespegue], [FechaArrivo], [HoraDespegue], [HoraArrivo], [TipoVuelo], [Escala], [EstadoVuelo], [CupoMax], [CupoActual]) VALUES (1, 1, 1, 3, N'10/10/2003', N'11/10/2003', N'09:00a.m', N'09:00p.m', N'Cabotaje', 0, N'Finalizado', 5, 1)
INSERT [dbo].[Vuelo] ([IDOrigen], [IDDestino], [IDAvion], [IDVuelo], [FechaDespegue], [FechaArrivo], [HoraDespegue], [HoraArrivo], [TipoVuelo], [Escala], [EstadoVuelo], [CupoMax], [CupoActual]) VALUES (1, 1, 1, 4, N'22/11/2020', N'22/11/2020', N'17/11/2020', N'17/11/2020', N'Cabotaje', 1, N'Disponible', 5, 0)
GO
INSERT [dbo].[Vuelo_Pasajero] ([IDPasajero], [IDVuelo]) VALUES (1, 1)
INSERT [dbo].[Vuelo_Pasajero] ([IDPasajero], [IDVuelo]) VALUES (2, 1)
INSERT [dbo].[Vuelo_Pasajero] ([IDPasajero], [IDVuelo]) VALUES (2, 2)
GO
ALTER TABLE [dbo].[Vuelo_Pasajero]  WITH CHECK ADD  CONSTRAINT [FK_Vuelo_Pasajero_Vuelo_Pasajero] FOREIGN KEY([IDPasajero], [IDVuelo])
REFERENCES [dbo].[Vuelo_Pasajero] ([IDPasajero], [IDVuelo])
GO
ALTER TABLE [dbo].[Vuelo_Pasajero] CHECK CONSTRAINT [FK_Vuelo_Pasajero_Vuelo_Pasajero]
GO
/****** Object:  StoredProcedure [dbo].[AVION_LISTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AVION_LISTAR]
as
BEGIN
SELECT * FROM Avion
END
GO
/****** Object:  StoredProcedure [dbo].[BUSCAR_VUELO]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BUSCAR_VUELO]
@IDVuelo int
AS
BEGIN
SELECT * FROM VUELO WHERE IDVuelo = @IDVuelo;
END
GO
/****** Object:  StoredProcedure [dbo].[LUGAR_BORRAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LUGAR_BORRAR]
@IDLugar int
as
BEGIN

DELETE FROM Lugar
where
IDLugar = @IDLugar 
END
GO
/****** Object:  StoredProcedure [dbo].[LUGAR_INSERTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LUGAR_INSERTAR]
@pais varchar(50), @ciudad varchar(50), @aeropuerto varchar(50), @latitud money, @longitud money
as
BEGIN
declare @IDLugar int
set @IDLugar = isnull((SELECT MAX(IDLugar) from Lugar), 0) +1

insert Lugar values (@IDLugar,@pais,@ciudad,@aeropuerto,@latitud,@longitud)
END
GO
/****** Object:  StoredProcedure [dbo].[LUGAR_LISTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LUGAR_LISTAR]
as
BEGIN
SELECT * FROM Lugar
END
GO
/****** Object:  StoredProcedure [dbo].[LUGAR_MODIFICAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LUGAR_MODIFICAR]
@IDLugar int, @Pais varchar(50), @Ciudad varchar(50), @Aeropuerto varchar(50), @Latitud money, @Longitud money
as
BEGIN

update LUGAR set
pais = @Pais,
ciudad = @Ciudad,
aeropuerto = @Aeropuerto,
latitud = @Latitud,
longitud = @Longitud
where
IDLugar = @IDLugar

END
GO
/****** Object:  StoredProcedure [dbo].[PASAJERO_BORRAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJERO_BORRAR]
@IDPasajero int
as
BEGIN

DELETE FROM Pasajero
where
IDPasajero = @IDPasajero 

DELETE FROM Vuelo_Pasajero
where
IDPasajero = @IDPasajero


update vuelo
set CupoActual = CupoActual - 1
where IDVuelo in ((select IDVuelo from vuelo_pasajero where IDPasajero = @IDPasajero))

END
GO
/****** Object:  StoredProcedure [dbo].[PASAJERO_INSERTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJERO_INSERTAR]
@dni int, @nom varchar(50), @apell varchar(50)
as
BEGIN
declare @IDPasajero int
set @IDPasajero = isnull((SELECT MAX(IDPasajero) from Pasajero), 0) +1

insert Pasajero values (@dni,@nom,@apell,@IDPasajero)
END
GO
/****** Object:  StoredProcedure [dbo].[PASAJERO_LISTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJERO_LISTAR]
AS
BEGIN
SELECT * FROM Pasajero
END
GO
/****** Object:  StoredProcedure [dbo].[PASAJERO_MODIFICAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJERO_MODIFICAR]
@IDPasajero int, @nom varchar(50), @apell varchar(50), @dni int
as
BEGIN

update PASAJERO set
nombre = @nom,
apellido = @apell,
dni = @dni
where
IDPasajero = @IDPasajero

END
GO
/****** Object:  StoredProcedure [dbo].[PASAJERO_VUELO]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJERO_VUELO]
@IDVuelo int
AS
BEGIN
SELECT * FROM PASAJERO
WHERE IDPASAJERO
IN (SELECT IDPasajero FROM VUELO_PASAJERO WHERE IDVUELO = @IDVuelo)
END
GO
/****** Object:  StoredProcedure [dbo].[PASAJEROVIAJE_INSERTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJEROVIAJE_INSERTAR]
@IDVuelo int, @IDPasajero int
AS
BEGIN

insert Vuelo_Pasajero values (@IDVuelo,@IDPasajero)

update Vuelo set
CupoActual = CupoActual+1
where
IDVuelo = @IDVuelo

END
GO
/****** Object:  StoredProcedure [dbo].[PASAJEROVIAJE_LISTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PASAJEROVIAJE_LISTAR]
AS
BEGIN
select * from vuelo_pasajero
END
GO
/****** Object:  StoredProcedure [dbo].[VUELO_ALTA]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VUELO_ALTA]
@IDOrigen int, @IDDestino int, @IDAvion int, @FechaDespegue varchar(50), @FechaArrivo varchar(50), @HoraDespegue varchar(50), @HoraArrivo varchar(50), @TipoVuelo varchar(50), @Escala bit, @EstadoVuelo varchar(50), @CupoMax int, @CupoActual int
AS
BEGIN
declare @IDVuelo int
set @IDVuelo = isnull((SELECT MAX(IDVuelo) from Vuelo), 0) +1

insert Vuelo values (@IDOrigen,@IDDestino,@IDAvion,@IDVuelo,@FechaDespegue,@FechaArrivo,@HoraDespegue,@HoraArrivo,@TipoVuelo,@Escala,@EstadoVuelo,@CupoMax,@CupoActual)
END
GO
/****** Object:  StoredProcedure [dbo].[VUELO_CANCELAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VUELO_CANCELAR]
@IDVuelo int
as
BEGIN

DELETE FROM Vuelo
where
IDVuelo = @IDVuelo

DELETE FROM Vuelo_Pasajero
where
IDVuelo = @IDVuelo

END
GO
/****** Object:  StoredProcedure [dbo].[VUELO_LISTAR]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VUELO_LISTAR]
AS
BEGIN
SELECT * FROM Vuelo
END
GO
/****** Object:  StoredProcedure [dbo].[VUELO_LISTAR_RECORTADO]    Script Date: 17/11/2020 08:45:37 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VUELO_LISTAR_RECORTADO]
AS
BEGIN
SELECT IDDestino,FechaDespegue,TipoVuelo FROM Vuelo
END
GO
USE [master]
GO
ALTER DATABASE [2doParcial-Aeropuerto-LUG] SET  READ_WRITE 
GO
